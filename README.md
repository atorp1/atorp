# atorp

### Convert Articy Draft X output to Ren'py scripts.


## Getting started

Run atorp.py from the command-line.  It requires no additional libraries, so as long as you have python 3 installed it should run.  Run it with no parameters specified to see the help.

``` python3 atorp.py ```

Generally you will probably want the default options, so you can just specify the manifest file name.  Export your project from Articy Draft as JSON.  Pass the file name of the manifest file (manifest.json) to the script:

``` python3 atorp.py manifest.json ```

If you are not running the script from the same directory as your data export you will need to pass the path to the manifest file.

``` python3 atorp.py /my_stuff/my_game/manifest.json ```

This will generate one or more Ren'py files as output.  This will be a basic runnable project.


## What it does

This script converts your Articy Draft JSON output to Ren'py code files.  The intended usage is a one-time conversion to generate the basic code which will then be further edited and customized to complete the project.  Not all features of Articy Draft are supported.  If you run the conversion again any edits you've made to the resulting Ren'py output will be overwritten and lost.  There is no provision for going the other direction (Ren'py to Articy).

Again, the expectation here is that you will use Articy as a visual design tool to start the project, but then once you move over to Ren'py you're done with Articy and any further changes will be made by editing the resulting Ren'py code.  This can help you quickly get the foundation of your game built visually and get straight to the more interesting bits quickly.


## Advanced options

There are some constants towards the top of the script, some of which affect the generated code.  You can edit these to suit your preferences, but they should work fine at their default values.  You may want to experiment with the COMMENT_VERBOSITY value to find the level of information that you prefer in the generated comments.

## Limitations

Dialogues and Dialogue Fragments are expected to only have one input pin.  This will not limit your ability to create complex stories in any way.  If you find that you need support fo more input pins please reach out with your use case and I'll see about getting support added.

Stage directions, if you use them, must be valid Ren'Py commands.

These limitations may cause the Articy Draft X sample projects to fail to convert, or fail to run in Ren'Py.  You can edit these sample projects to fit the limitations and they will then run.
