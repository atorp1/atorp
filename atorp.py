from os import path
import re
import json
import logging
import string
from functools import cached_property
from argparse import ArgumentParser
from collections import defaultdict


logging.basicConfig(level=logging.DEBUG)


INDENT = '    '
INDENT_LIMIT = 50
ENTITY_TYPE_NAMES = ['Entity', 'DefaultMainCharacterTemplate', 'DefaultSupportingCharacterTemplate', 'Characters_f', 'Characters']
COMMENT_VERBOSITY = 2   # 0-3 for commments inserted in generated code.  Higher is more
PREVIEW_LENGTH = 15   # For jump comments that show preview text of the target
# pylint: disable=missing-function-docstring, no-member, line-too-long, missing-class-docstring, trailing-newlines, logging-fstring-interpolation, protected-access, unspecified-encoding

#region Utils


def convert_code(code, single_line=True):
    ''' Convert c/c# style code to Python using some simple replacements.  '''
    if not code:
        return code
    # Ensure there's some actual code here and not just whitespace and/or comments
    lines = (l.strip() for l in code.split('\n'))
    lines = [l for l in lines if l and not l.startswith('//')]
    if not lines:
        return '' if single_line else ['', ]
    pycode = ' '.join(lines)

    lang_map = (
        ('true', 'True'),
        ('false', 'False'),
        ('&&', 'and'),
        ('||', 'or'),
    )
    pylines = []
    for line in lines:
        for old, new in lang_map:
            line = line.replace(old, new)
        line = re.sub(r'!([^=])', r'not \1', line)  # Convert ! to "not" without breaking !=
        pylines.append(line)

    pycode = ' '.join(pylines)
    logging.debug(f'Code converted {code} -> {pycode}')

    return pycode if single_line else pylines


def safe_ident(name):
    ''' Remove invalid characters from names, etc. so they can be used as identifiers such as labels. '''
    return re.sub(r'[^\w\d]*', '', name)


def get_dict_value(data, key, msg):
    value = data.get(key)
    assert value is not None, msg
    return value


def preview_text(value):
    ''' Generate a short previw of text, used in comments. '''
    if not value:
        return ''
    if len(value) > PREVIEW_LENGTH:
        return f'{value[:PREVIEW_LENGTH]}...'
    return value


def get_var_name(name):
    ''' Get variable name from entity name.  Mostly just lowercase the first word and remove invalid chars. '''
    if len(name) == 0:
        return name
    var_name = ''.join(x for x in name.split(' ')[0] if x in string.ascii_letters + string.digits).lower()
    if var_name[0].isdigit():
        var_name = 'ent_' + var_name
    return var_name


#endregion Utils


#region main


class Entities:
    def __init__(self):
        self._entities = []
        self._id_map = {}

    def __iter__(self):
        return self._entities.__iter__()

    def __len__(self):
        return len(self._entities)

    def load_from_dict(self, object_data):
        for item in object_data:
            object_type_name = get_dict_value(item, 'Type', 'object has no type')
            if object_type_name not in ENTITY_TYPE_NAMES:
                continue
            props = get_dict_value(item, 'Properties', 'entity missing properties')
            self._entities.append(Entity(props['Id'], props['DisplayName']))
        self._id_map = {x.unique_id: x for x in self._entities}

    def find(self, unique_id):
        ent = self._id_map.get(unique_id)
        if ent is not None:
            return ent

        # Only treat this as an error if the id is not zero
        method = logging.debug if unique_id and int(unique_id, 16) == 0 else logging.error
        method('Unable to find entity with id: %s', unique_id)
        return None


class Entity:
    ''' Represents an entity (usually a named character) in the story. '''
    def __init__(self, unique_id, display_name):
        self.unique_id = unique_id
        self.var_name = get_var_name(display_name)
        if display_name.lower() == 'narrator':
            display_name = ''
        self.display_name = display_name

    def __bool__(self):
        return len(self.var_name) > 0

    def __str__(self):
        return self.var_name


class SourceObjects:
    ''' A container for all of the objects that make up the main part of the project, excluding entities.
    '''
    def __init__(self, entities):
        self._objects = []
        self._entities = entities
        self.menu_captions = False

    def __iter__(self):
        return self._objects.__iter__()

    # def __next__(self):
    #     print('This shouldnt happen')

    def __len__(self):
        return len(self._objects)

    def load_from_dict(self, object_data):
        for item in object_data:
            obj = create_object(self, item)
            if obj is None:
                continue
            self._objects.append(obj)
        self.create_friendly_ids()   # Remove this to keep original ids in output; may be helpful for debugging

    def find(self, unique_id):
        for obj in self._objects:
            if obj.unique_id == unique_id:
                return obj

        # logging.error('Unable to find object with id: %s', unique_id)
        return None

    def create_friendly_ids(self):
        ''' Replace the default hex object ids with ids that indicate the object type and have
            sequential numbers, which will hopefully be more human-friendly.
        '''
        id_map = {}
        id_counter = 0
        ndigits = len(str((len(self)))) + 1
        # First pass - assign ids
        for obj in self:
            id_counter += 1
            new_id = f'{obj.short_name}{str(id_counter).zfill(ndigits)}'
            id_map[obj.unique_id] = new_id
            obj.unique_id = new_id
        # Second pass - assign new ids to all fields
        for obj in self:
            obj.parent_id = id_map.get(obj.parent_id, obj.parent_id)
            obj.input_pin.connections = [id_map.get(val, val) for val in obj.input_pin.connections]
            obj.output_pin.connections = [id_map.get(val, val) for val in obj.output_pin.connections]

    def __str__(self):
        retval = f'{self.__class__.__name__}: {len(self._objects)} objects'
        if logging.root.level == logging.DEBUG:
            retval += '\n  ' + '\n  '.join(str(obj).replace('\n', '\n  ') for obj in self._objects)
        return retval


def create_object(root, data):
    object_type_name = get_dict_value(data, 'Type', 'object has no type')
    cls = OBJECT_CLASS_MAP.get(object_type_name)
    if cls is None:
        logging.debug('Ignoring unsupported object of type %s', object_type_name)
        return None
    properties = get_dict_value(data, 'Properties', 'object missing properties')

    obj = cls(root, properties)
    return obj


def find_final_target(target):
    ''' If a chile's output pin links to its parent, the ultimate target is the parent's output pin's target '''
    target_id = target.unique_id
    connections = target.output_pin.connections
    if len(connections) == 0:
        logging.error(f'Parent {target_id} has no outgoing connections')
        return target
    if len(connections) > 1:
        logging.warning(f'Parent {target_id} has multiple outgoing connections.  Using the first.')
    next_id = connections[0]
    next_target = target.root.find(next_id)
    if next_id == target.parent_id:
        return find_final_target(next_target)  # Travel up the chain
    return next_target


def create_jump(target, indent):
    retval = f'{indent}jump {target.get_full_path()}'
    if COMMENT_VERBOSITY >= 1:
        hint = target.get_hint()
        if hint:
            retval += f'   # {hint}'
    return retval


class Pin:
    def __init__(self):
        self.code = ''
        self.connections = []


class BaseObject:
    ''' Base class that represents an object read from a source JSON package.
        Loads properties from a dict to object properties that can be accessed using dot notation.
    '''
    type_name = '__base__'
    short_name = '__err__'

    def __init__(self, root, properties):
        self._custom_props = []  # Names of custom properties added from source JSON file
        self.root = root
        self.input_pin = Pin()
        self.output_pin = Pin()
        self.load_properties(properties)

    def _add_custom_prop(self, name, value):
        setattr(self, name, value)
        self._custom_props.append(name)

    def _prop_load(self, properties, prop_map):
        for source, dest in prop_map.items():
            value = properties.get(source)
            if value is None:
                logging.warning('missing %s property', source)
            self._add_custom_prop(dest, value)

    def get_short_name(self):
        return safe_ident(self.unique_id)

    def get_hint(self):
        return ''

    @property
    def condition(self):
        return self.input_pin.code

    @property
    def target_ids(self):
        if self.input_pin.connections:
            return self.input_pin.connections
        return self.output_pin.connections

    @property
    def instruction(self):
        return self.output_pin.code

    def get_full_path(self):
        ancestors = [self, ]
        current = self
        while len(ancestors) < 20:  # Sanity check, avoid infinite recursion just in case.  Probably can't happen.
            current = self.root.find(current.parent_id)
            if current is None:
                break
            ancestors.append(current)

        return '_'.join(a.get_short_name() for a in reversed(ancestors))

    def load_properties(self, properties):
        name_map = {
            'Id': 'unique_id',
            'Parent': 'parent_id',
        }
        self._prop_load(properties, name_map)
        input_pins = properties.get('InputPins')
        if input_pins is not None:
            assert len(input_pins) < 2, f'Too many input pins for id {self.unique_id}'
            self.input_pin.code = convert_code(input_pins[0]['Text'])
            self.input_pin.connections = [conn['Target'] for pin in input_pins for conn in pin.get('Connections', [])]
        out_pins = properties.get('OutputPins')
        if out_pins is not None:  # Jumps won't have this
            # assert len(out_pins) < 2, f'Too many output pins for id {self.unique_id}'
            self.output_pin.code = convert_code(out_pins[0]['Text'], single_line=False)
            self.output_pin.connections = [conn['Target'] for pin in out_pins for conn in pin.get('Connections', [])]
        else:
            # instruction = ''
            target = properties.get('Target')
            if target is not None:
                self.output_pin.connections = [target, ]
        self._add_custom_prop('x_pos', properties['Position']['x'])
        self._add_custom_prop('y_pos', properties['Position']['y'])

    def incoming_link_count(self):
        result = sum(int(self.unique_id in obj.target_ids) for obj in self.root)
        return result

    def _min_path_length(self, exclude=None):
        if exclude is None:
            exclude = [self, ]
        elif self in exclude:
            return None
        else:
            exclude.append(self)
        links = []
        for x in (x for x in self.root if self.unique_id in x.target_ids):
            val = x._min_path_length(exclude)
            if val is not None:
                links.append(val)
        # links = [
        #     # self.root.find(x.unique_id).path_length
        #     x.path_length
        #     for x in self.root
        #     if self.unique_id in x.target_ids
        # ]
        if len(links) == 0:
            logging.debug('Object has no incoming links: %s', self)
            return 0
        return 1 + min(links)

    @cached_property
    def path_length(self):
        return self._min_path_length()

    def _export_content(self, output, saved, indent):
        pass

    def _include_sibling(self, sibling, output, saved, indent):
        if sibling.incoming_link_count() < 2 and len(indent) < INDENT_LIMIT:
            if sibling in saved:
                # logging.debug(f'Quitting on {sibling.unique_id} to {self.unique_id}')
                out_str = create_jump(sibling, indent)
                output.append(out_str)
                return
            # logging.debug('appending from %s to %s', sibling.unique_id, self.unique_id)
            sibling_output = sibling.export_lines(saved, True, indent)
            output.extend(sibling_output)
        else:
            if sibling.condition:
                output.append(f'{indent}if {sibling.condition}:')
                indent += INDENT
            out_str = create_jump(sibling, indent)
            output.append(out_str)
            # output.append('')

    def export_lines(self, saved, append=False, indent=INDENT):
        output = []
        saved.append(self)
        if append:
            if self.condition:
                output.append(f'{indent}if {self.condition}:')
                indent += INDENT
        else:
            logging.debug('label: %s', self.get_full_path())
            output.append(f'label {self.get_full_path()}:')
        self._export_content(output, saved, indent)
        if len(self.target_ids) == 0:
            output.append(f'{indent}return')
        elif len(self.target_ids) == 1:
            target_id = self.target_ids[0]
            target = self.root.find(target_id)
            if target is None:
                logging.debug(f'Unable to find target for {self.unique_id}: {target_id}')
                return output
            if target_id == self.parent_id:
                # When a child's outgoing connection targets its parent, instead use the parent's connections
                next_target = find_final_target(target)
                val = create_jump(next_target, indent)
                output.append(val)
            else:
               self._include_sibling(target, output, saved, indent)
        if not append:
            output.append('')
        return output

    def __str__(self):
        retval = f'{self.__class__.__name__} ({self.unique_id})'
        if logging.root.level == logging.DEBUG:
            retval += '\n  ' + '\n  '.join(f'{name}: {getattr(self, name)}' for name in self._custom_props) + '\n'
        return retval


class BaseContainer(BaseObject):
    type_name = '__base_object__'
    short_name = '__err2__'

    def load_properties(self, properties):
        super().load_properties(properties)
        name_map = {
            'DisplayName': 'display_name',
        }
        self._prop_load(properties, name_map)

    def get_short_name(self):
        if self.display_name:
            return safe_ident(self.display_name.replace(' ', '_'))
        return super().get_short_name()


class FlowFragment(BaseContainer):
    type_name = 'FlowFragment'
    short_name = 'FlFr'

    def _min_path_length(self, exclude=None):
        return 0

    def export_lines(self, saved, append=False, indent=INDENT):
        if self != self.root._objects[0]:
            return super().export_lines(saved, append, indent)

        # This is the base of the entire project, treat it special.
        assert not append, 'Appending project base to what?'
        output = []
        saved.append(self)
        logging.debug('label: start for base %s', self.type_name)
        output.append('label start:')
        # Find the top-left child of this parent
        items = [x for x in self.root if x.parent_id == self.unique_id]
        assert len(items) > 0, 'Project base node has no children.'
        items.sort(key=lambda itm: (itm.x_pos, itm.y_pos))
        output.append(f'{indent}jump {items[0].get_full_path()}')
        output.append('')
        return output



class Dialogue(BaseContainer):
    type_name = 'Dialogue'
    short_name = 'Dlg'

    def _min_path_length(self, exclude=None):
        return 0


class DialogueFragment(BaseObject):
    type_name = 'DialogueFragment'
    short_name = 'DlgFr'

    def get_hint(self):
        return preview_text(self.text)

    def load_properties(self, properties):
        super().load_properties(properties)
        self.speaker = None  # Avoid linter error
        name_map = {
            'Speaker': 'speaker',
            'Text': 'text',
            'StageDirections': 'stage_directions',
            'MenuText': 'menu_text',
        }
        self._prop_load(properties, name_map)
        if self.speaker:
            speaker_name = self.speaker
            self.speaker = self.root._entities.find(speaker_name)
            if self.speaker is None:
                logging.debug('Speaker with name "%s" not found', speaker_name)
                self.speaker = Entity(speaker_name, '')
        else:
            logging.debug('DFrag %s has no speaker', self)
            self.speaker = None
        self.stage_directions = self.stage_directions.split(';') if self.stage_directions else []

    def _is_menu(self, targets):
        ''' Does a list of target elements represent a menu? '''
        has_menu = False
        has_nonmenu = False
        for target in targets:
            has_nonmenu = has_nonmenu or target.type_name != DialogueFragment.type_name or len(target.menu_text) == 0
            has_menu = has_menu or bool(target.menu_text)

        if has_menu and has_nonmenu:
            raise ValueError(f'Element has both menu and non-menu links: {self}')
        assert has_menu or has_nonmenu, 'Element appears to have no links.  This should never happen.'
        return has_menu

    def _export_content(self, output, saved, indent=INDENT):
        if self.stage_directions:
            for stdir in self.stage_directions:
                output.append(f'{indent}{stdir}')
        if self.instruction:
            for line in self.instruction:
                output.append(f'{indent}$ {line}')
        main_text = ''
        if self.text:
            text_pre = f'{indent}{self.speaker} ' if self.speaker else indent
            text = self.text.replace('\n', r'\n').replace('\r', '')
            main_text = f'{text_pre}"{text}"'
            if COMMENT_VERBOSITY >= 3:
                main_text += f'   # {self.unique_id}'

        if len(self.target_ids) < 2:
            output.append(main_text)
        else:
            targets = [self.root.find(tid) for tid in self.target_ids]
            targets.sort(key=lambda obj: obj.y_pos)
            if self._is_menu(targets):
                if main_text and not self.root.menu_captions:
                    output.append(main_text)
                output.append(f'{indent}menu:')
                if main_text and self.root.menu_captions:
                    output.append(f'{INDENT}{main_text}')
                for target in targets:
                    condition = f' if {target.condition}' if target.condition else ''
                    out_str = f'{indent}{INDENT}"{target.menu_text}"{condition}:'
                    if COMMENT_VERBOSITY >= 3:
                        out_str += f'   # {target.unique_id}'
                    output.append(out_str)
                    self._include_sibling(target, output, saved, indent + INDENT * 2)
            else:
                output.append(main_text)
                for target in targets:
                    self._include_sibling(target, output, saved, indent)


class Instruction(BaseObject):
    type_name = 'Instruction'
    short_name = 'Inst'

    def get_hint(self):
        return f'Expr: {preview_text(self.expression)}'

    def load_properties(self, properties):
        super().load_properties(properties)
        name_map = {
            'Expression': 'expression',
        }
        self._prop_load(properties, name_map)
        self.expression = convert_code(self.expression, single_line=False)

    def _export_content(self, output, saved, indent=INDENT):
        for line in self.expression:
            output.append(f'{indent}$ {line}')


class Condition(BaseObject):
    type_name = 'Condition'
    short_name = 'Cond'

    def get_hint(self):
        return f'Cond: {preview_text(self.expression)}'

    def load_properties(self, properties):
        super().load_properties(properties)
        name_map = {
            'Expression': 'expression',
        }
        self._prop_load(properties, name_map)
        self.expression = convert_code(self.expression)
        assert len(self.target_ids) == 2, f'Condition {self.unique_id} has incorrect target count: {len(self.target_ids)}'

    def _export_content(self, output, saved, indent=INDENT):
        if COMMENT_VERBOSITY >= 2:
            output.append(f'{indent}# Condition {self.unique_id}')
        expr = self.expression
        if not expr:
            logging.warning(f'Condition {self.unique_id} has no expression.  Defaulting to True')
            expr = 'True'
        output.append(f'{indent}if {expr}:')
        target1 = self.root.find(self.target_ids[0])
        target2 = self.root.find(self.target_ids[1])
        if target1 is None:
            logging.debug(f'Unable to locate target1 for {self.unique_id}: {self.target_ids[0]}')
            return
        if target2 is None:
            logging.debug(f'Unable to locate target2 for {self.unique_id}: {self.target_ids[1]}')
            return
        self._include_sibling(target1, output, saved, indent + INDENT)
        output.append(f'{indent}else:')
        self._include_sibling(target2, output, saved, indent + INDENT)


class Hub(BaseObject):
    type_name = 'Hub'
    short_name = 'Hub'

    def get_hint(self):
        return 'Hub'

    def load_properties(self, properties):
        super().load_properties(properties)
        name_map = {
            'DisplayName': 'display_name',
        }
        self._prop_load(properties, name_map)

    def _export_content(self, output, saved, indent=INDENT):
        if COMMENT_VERBOSITY >= 2:
            output.append(f'{indent}# HUB {self.display_name}({self.unique_id})')


class Jump(BaseObject):
    type_name = 'Jump'
    short_name = 'Jump'

    def _export_content(self, output, saved, indent=INDENT):
        if COMMENT_VERBOSITY >= 2:
            output.append(f'{indent}# JUMP({self.unique_id}) -> {self.target_ids[0]}')



OBJECT_CLASSES = [FlowFragment, Dialogue, DialogueFragment, Instruction, Condition, Hub, Jump, ]
OBJECT_CLASS_MAP = {cls.type_name: cls for cls in OBJECT_CLASSES}
OBJECT_CLASS_MAP['DialogueLine'] = DialogueFragment
OBJECT_CLASS_MAP['DialogueLine_f'] = DialogueFragment


#endregion main


#region Save/Load


def deref_objects(object_data, texts, lang_code='en'):
    ''' Copy localized text values to their sources, removing one level of indirection. '''
    def fixup_val(props, name):
        value = props.get(name)
        if not value:
            return
        thing = texts.get(value)
        if thing is None:
            return

        thing = thing.get('') or thing.get(lang_code)
        if thing is None:
            return   # No text available
        new_value = thing['Text']

        # logging.debug('Replacing %s with %s', value, new_value)
        props[name] = new_value

    for datum in object_data:
        props = get_dict_value(datum, 'Properties', 'Object missing properties')
        fixup_val(props, 'DisplayName')
        fixup_val(props, 'Text')


def load(filename):
    file_path = path.dirname(filename)
    with open(filename, 'r') as f:
        root = json.load(f)
    with open(path.join(file_path, root['GlobalVariables']['FileName']), 'r') as f:
        global_vars = json.load(f)
    packages = root['Packages']
    if len(packages) != 1:
        logging.error('Expected packages to have one item, instead it had %s', len(packages))
        raise ValueError('Packages count')
    with open(path.join(file_path, packages[0]['Files']['Objects']['FileName']), 'r') as f:
        package = json.load(f)
    with open(path.join(file_path, packages[0]['Files']['Texts']['FileName']), 'r') as f:
        texts = json.load(f)
    object_data = get_dict_value(package, 'Objects', 'Not a valid package.')
    deref_objects(object_data, texts)  # Replace text ids with actual text so we don't have to look them up later
    entities = Entities()
    entities.load_from_dict(object_data)
    objs = SourceObjects(entities)
    objs.load_from_dict(object_data)
    values = {
        'objects': objs,
        'vars': global_vars['GlobalVariables'],
    }
    return values


def save_entities(ent_data):
    output = []
    for entity in ent_data:
        ent_name = entity.display_name
        var_name = entity.var_name
        output.append(f'define {var_name} = Character("{ent_name}")')

    output.append('\n')
    return '\n'.join(output)


def save_vars(var_data):
    output = []
    for namespace in var_data:
        name = namespace['Namespace']
        vars = namespace['Variables']
        for var in vars:
            val = var["Value"]
            if val == '' and var['Type'] == 'String':
                val = '""'
            line = f'default {name}.{var["Variable"]} = {val}'
            desc = var['Description']
            if desc:
                output.append(f'{line}   # {desc}')
            else:
                output.append(line)

    output.append('\n')
    return '\n'.join(output)


def save_object(obj, output, saved):
    if obj in saved:
        return
    saved.append(obj)
    obj_lines = obj.export_lines(saved)
    output.extend(obj_lines)
    for target_id in obj.target_ids:
        target = obj.root.find(target_id)
        if not target:
            logging.warning('Target object not found for id %s', target_id)
            continue
        save_object(target, output, saved)


def save_objects(objs, saved=None):
    if saved is None:
        saved = []
    output = []
    objs = sorted(objs, key=lambda x: x.path_length)
    for obj in objs:
        save_object(obj, output, saved)

    # print('...')
    # print(output)

    return '\n'.join(output)


def save_single(game_data, file_path='', saved=None):
    ents_str = save_entities(game_data['objects']._entities)
    vars_str = save_vars(game_data['vars'])
    obj_str = save_objects(game_data['objects'], saved)

    filename = path.join(file_path, 'script.rpy')
    logging.info(f'Saving to {filename}')
    with open(filename, 'w') as f:
        f.write(ents_str)
        f.write(vars_str)
        f.write(obj_str)


def save_multi(game_data, file_path=''):
    objs = game_data['objects']
    dialogues = [x for x in objs if x.type_name == 'Dialogue']
    file_names = set()
    saved = []
    # Write each dialogue and children to its own file
    for dialogue in dialogues:
        items = [x for x in objs if x.parent_id == dialogue.unique_id]
        obj_str = save_objects([dialogue, ] + items, saved)
        file_name = dialogue.display_name.lower().replace(' ', '_') + '.rpy'
        # If two dialogues have the same display name that will break things.  Not sure if it's possible but just in case...
        # TODO: Check if it's possible
        if file_name in file_names:
            raise ValueError(f'Duplicate file name.  {file_name} already used.')
        file_names.add(file_name)
        full_path = path.join(file_path, file_name)
        logging.info('Saving to %s', full_path)
        with open(full_path, 'w') as f:
            f.write(obj_str)

    # Write whatever remains to the main file
    save_single(game_data, file_path, saved)


#endregion Save/Load


# TODO: Code clean-up
# TODO: Add option for choosing menu captions ordering



def get_args():
    parser = ArgumentParser(description='Convert Articy-exported JSON files to Ren\'py scripts.')
    parser.add_argument('manifest_filename')
    parser.add_argument('-o', '--output-dir', default='')
    parser.add_argument('-s', '--single-file', action='store_true',
                        help='Create a single output file in the same directory as the source file.')
    parser.add_argument('-c', '--menu-captions', action='store_true',
                        help='Include dialog text as a caption inside a menu.')
    return parser.parse_args()


def main():
    args = get_args()
    logging.info('Loading from %s...', args.manifest_filename)
    logging.info('Output directory: %s', args.output_dir)
    logging.info('Output dialog text as menu captions where applicable: %s', args.menu_captions)
    game_data = load(args.manifest_filename)
    logging.info('Loaded %s objects', len(game_data['objects']))
    logging.info('Loaded %s entities', len(game_data['objects']._entities))
    if not path.isdir(args.output_dir):
        print(f'Output directory {args.output_dir} does not exist.')
        return
    game_data['objects'].menu_captions = args.menu_captions
    if args.single_file:
        save_single(game_data, args.output_dir)
    else:
        save_multi(game_data, args.output_dir)


if __name__ == '__main__':
    main()



